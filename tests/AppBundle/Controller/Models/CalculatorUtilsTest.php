<?php
namespace Tests\AppBundle\Controller\Models;


use AppBundle\Controller\Models\CalculatorUtils as Calc;


class CalculatorTest extends \PHPUnit_Framework_TestCase
{
//    function testCheckIfphpUnitIsWorkingFine() {
//        $this->assertTrue(true);
//    }

public function setInputData() {
    return array(
        array(
            "a" => 1,
            "b" => 2,
            "total" => 3,
      ),
        array(
            "a" => 2,
            "b" => 2,
            "total" => 4,
      ),
        array(
            "a" => 6,
            "b" => 4,
            "total" => 10,
      ),
    );
}

    /**
     *  @expectedException \InvalidArgumentException
     */
    function testThrowAnExceptionIfNonNumber() {
        $calc = new Calc();
        $calc->add(2,'a1');
    }

    /**
     * @dataProvider setInputData
     */
    function testIsPossibleToAddToNumbers($a, $b, $expectedValue) {
        $calc = new Calc();
        $this->assertEquals($expectedValue, $calc->add($a, $b));
    }

    /**
     * @expectedExceptionMessage Cannot Divide By zero!
     */
    public function testIsThrownExceptionDivisionByZero() {

     }

    /**
     * @expectedException \PHPUnit_Framework_Error
     */
    public function testFailingInclude()
    {
        include 'not_existing_file.php';
    }
}
