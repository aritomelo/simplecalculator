<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
// Custom validation
use AppBundle\Validator\Constraints as CalculatorAssert;

/**
 * @CalculatorAssert\CheckOperand2OperatorFields
 */
class Calculator
{
    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @Assert\Type("float")
     */
    private $operand1;
    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @Assert\Type("float")
     */
    private $operand2;
    /*
     * @ORM\Column(type="string")
     */
    private $operator;
    /*
     * @ORM\Column(type="float", length=150)
     */
    private $result;

    Const VALID_OPERATOR = array(
        '+' => "add",
        '-' => "subtraction",
        '/' => "division",
        '*' => "multiplication",
    );

    /**
     * @return mixed
     */
    public function getOperand1()
    {
        return $this->operand1;
    }

    /**
     * @param mixed $operand1
     */
    public function setOperand1($operand1)
    {
        $this->operand1 = $operand1;
    }

    /**
     * @return mixed
     */
    public function getOperand2()
    {
        return $this->operand2;
    }

    /**
     * @param mixed $operand2
     */
    public function setOperand2($operand2)
    {
        $this->operand2 = $operand2;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param mixed $operator
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
    }

    /**
     * Convert object Calculator into an array.
     *
     * @param Calculator    $calc
     * @return array
     */
    public static function calculatorIntoArray(Calculator $calc)  {
        $calcArray = array();

        if (!is_null($calc)) {
            $calcArray = array(
                'operand1'  => $calc->getOperand1(),
                'operand2'  => $calc->getOperand2(),
                'operator'  => $calc->getOperator(),
                'result'    => $calc->result,
            );
        }

        return $calcArray;
    }
}