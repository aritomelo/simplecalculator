<?php

namespace AppBundle\Form;

// Main form, form AbstractType class
use Symfony\Component\Form\AbstractType;
// selector form field
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Calculator;

// Form builder interface - Allow Create a new form.
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CalculatorType extends AbstractType {

    // Our version of buildForm
    public function buildForm(FormBuilderInterface $builder, array $options) {
        // Operator selector
        $operatorSelect = array('choices'  => Calculator::VALID_OPERATOR);

        // Adding fields to our form.
        $builder
            ->add('operand1')
            ->add('operand2')
            ->add('operator' , ChoiceType::class, $operatorSelect)
            ->add('result')
            ->add('submit', SubmitType::class, array('label' => "="));
    }

     /**
     * Set some options to tell Which entity or data class this form will use
     *
     * @param OptionsResolver   $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver) {
        // $resolver->setDefaults(array('data_class' => 'Calculator\CalculatorBundle\Entity\Calculator'));
        $resolver->setDefaults(array(
            'data_class' => Calculator::class,
        ));
    }

    // return unique Name for a formtype, the name pattern should be, vendor_bundleName_objectName_form
    public function getName() {
        return 'Calculator_calculatorBundle_calculator';
    }
}