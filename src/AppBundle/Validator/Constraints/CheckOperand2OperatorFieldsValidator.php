<?php
/**
 * Created by PhpStorm.
 * User: melo
 * Date: 16/03/2017
 * Time: 10:53
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CheckOperand2OperatorFieldsValidator extends ConstraintValidator
{
    public function validate($calculator, Constraint $constraint)
    {
        if (($calculator->getOperand2() == 0) && ($calculator->getOperator() == 'division')) {
            $this->context->buildViolation($constraint->message)
                ->atPath('operand2')
                ->addViolation();
        }
    }
}