<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"ALL", "METHOD", "ANNOTATION"})
 */
class CheckOperand2OperatorFields extends Constraint
{
    public $message = 'Field Operand2 cannot be zero.';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}