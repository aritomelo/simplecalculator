<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Form\CalculatorType;
use AppBundle\Entity\Calculator;
use AppBundle\Controller\Models\CalculatorUtils as calcUtils;

class CalculatorController extends Controller
{

    private $headerTitle = "Simple Calculator";
    /**
     * @Route("/", name="calculator")
     */
    public function calculatorAction(Request $request)
    {
        // Create a new calculator instance.
        $calc = new Calculator();

        // Get the form
        $form = $this->createFormVars($calc, 'calculator_result');

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);

        // Passing arrays of vars to the template.
        $vars = array(
            'form' => $form->createView(),
            'heading' => $this->headerTitle,
        );

        return $this->render('calculator/calculator.html.twig', $vars);
    }

    /**
     * @Route("/calculator", name="calculator_result")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function computeAction(Request $request)
    {
        // Create a new calculator instance.
        $calc = new Calculator();
        // 1) build the form
        $form = $this->createForm(CalculatorType::class, $calc);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $form = $this->createFormVars($calc, 'calculator_result');
            $calc->setResult($this->computeCalculus($calc));
            // Passing the form to the template.
            $vars = array(
                'form' => $form->createView(),
                'calcData' => Calculator::calculatorIntoArray($calc),
            );
        }

        // !validation
        $this->get('session')->getFlashBag()->add('msg', 'Something went wrong!');
        $vars = array(
            'form' => $form->createView(),
            'calcData' => Calculator::calculatorIntoArray($calc),
        );


        return $this->render('calculator/calculator.html.twig', $vars);
    }

    // Auxiliary function
    /**
     * Performance the calculus based on operator after its validation.
     *
     * @param Calculator    $calc
     * @return float|int|mixed
     */
    private function computeCalculus(Calculator $calc) {
        $calcOperator = $calc->getOperator();
        $result = 0;
        if (calcUtils::isValidOperator($calcOperator)) {
            switch ($calcOperator) {
                case 'division':
                    $result = calcUtils::divide($calc->getOperand1(), $calc->getOperand2());
                    break;

                case 'multiplication':
                    $result = calcUtils::multiplication($calc->getOperand1(), $calc->getOperand2());
                    break;

                case 'add':
                    $result = calcUtils::add($calc->getOperand1(), $calc->getOperand2());
                    break;

                case 'subtraction':
                    $result = calcUtils::subtraction($calc->getOperand1(), $calc->getOperand2());
                    break;

                default:
                    break;
            }
        }
        return $result;
    }

    /**
     * Implementation of method to create a form.
     *
     * @param Calculator        $calc
     * @param string            $path
     * @return \Symfony\Component\Form\Form
     */
    private function createFormVars($calc = null, $path = 'calculator_result' ) {
        $options = array(
            // path to go after reach submit button
            'action' => $this->generateUrl($path),
            'method' => 'POST',
        );
        // Creating a new form.
        $form = $this->createForm(CalculatorType::class, $calc, $options);

        return $form;
    }


}
