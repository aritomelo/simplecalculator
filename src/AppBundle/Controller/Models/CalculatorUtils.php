<?php
/**
 * Created by PhpStorm.
 * User: melo
 * Date: 15/03/2017
 * Time: 14:39
 */

namespace AppBundle\Controller\Models;
use AppBundle\Entity\Calculator;

class CalculatorUtils
{
    /* Operators available */
    private static $validOperatorsSign = array (
        "+",
        "*",
        "/",
        "-",
    );

   /**
    * The operators must be numeric
    *
    * @param float          $a
    * @param float          $b
    *
    * @return float|int
    */
    public static function validOperators($a, $b) {
        if (!is_numeric($a) || !is_numeric($b)) {
            throw new \InvalidArgumentException;
        }
        return TRUE;
    }

    /**
     * Implementation division.
     *
     * @param float     $a
     * @param float     $b
     *
     * @return float|int
     *
     * @throws \Exception
     */
    public static function divide($a, $b) {
        self::validOperators($a, $b);

        if ($b == 0) {
            throw new \Exception('Cannot Divide By zero!');
        }


        return $a/$b;
    }

    public static function add($a, $b) {
        self::validOperators($a, $b);

        return $a + $b;
    }

    public static function subtraction($a, $b) {
        self::validOperators($a, $b);

        return $a - $b;
    }

    public static function multiplication($a, $b) {
        self::validOperators($a, $b);

        return $a * $b;
    }
    
    public static function isValidOperator ($operator) {
        if (in_array($operator, self::$validOperatorsSign)) {
            throw new \Exception('Not a Valid operator, must be one of: +, *, -, /');
        }

        return TRUE;
    }
}