Arito Lima Melo (Backend PHP | Symfony | Drupal Web developer)
===============================================================
![calculatorV1.png](https://bitbucket.org/repo/5qx7aKo/images/2804393152-calculatorV1.png)

Simple Calculator
=================
 - Provides a calculator with basics operand (plus, minus, m) using Symfony 3.2.* and with basic styling using bootstrap template.


Table of Contents
=================
 
 * [Project Setup](#project-setup)
    * [Backend](#backend)
    * [Frontend](#frontend)
    * [Design Pattern](#design-pattern)
        * [Generate SASS Files](#generate-sass-files)
        * [Generate JS Files](#generate-js-files)
 * [Testing](#testing)
 * [Future work](#future-works)
 * [Tools](#tools)
     

Project Setup
=============

### Backend
 - Clone project (bitbucket, under request to aritomelo@gmail.com)

```

 - Open the project folder `simplecalculator`
```sh
$ cd simplecalculator
```

 -  Install project
```sh
$ composer install
```

### Frontend
In the `template` folder you can find all necessary files for  templating.

### Design Pattern
 - An api class to represent the Calculator controller
 - An util class called CalculatorUtils where all the calculator functionality takes place
 - A form implemented as a class called "CalculatorType" which extend AbstractType
 - An entity to represent the calculator (class Calculator)
 - A custom validation(extending ConstraintValidator and Constraints classes) for checking when the user tries to divide a number by zero,
 - Most of validation are done via annotation.


#### Generate Sass files (not implemented)


#### Generate JS files (not implemented)
To be JavaScript files are generated using something like TypeScript 2 or Grunt


### Testing

You can use Codeception to run/generate tests for Symfony.

```sh
$ phpunit tests/AppBundle/
```

### Future works
 - More complex features (scientific calculator, more user friendly)
 - Service and Cache management to speed up the response (like keep history of previous calculus)
 - PHPUnit : use mocks to test classes
 - Dependency Injection
 - Sass integration
 - Grunt for javaScript


### Tools (software/IDE)
 - Symfony 3.2.2
 - PhpStorm2016
 - PHP7.0.8